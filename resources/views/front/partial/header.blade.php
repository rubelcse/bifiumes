<!-- Bootstrap core CSS -->
<link href="{{asset('front/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

<!-- Custom styles for this template -->
<style>
    body {
        padding-top: 54px;
    }
    @media (min-width: 992px) {
        body {
            padding-top: 56px;
        }
    }

</style>